##  lucence项目简介

- 对lucence的基础的封装 简化操作
- 使用Maven对项目进行模块化管理，提高项目的易开发性、扩展性。

## 主要功能
 1. 提供工具类 方便操作
 2. reader、writer searcher 的util。
 3. test 目录有测试例子。
 4. 测试用例在src/main/test 

## 技术选型
    ● 核心框架：lucence 5.5
    ● 日志管理：SLF4J、Log4j2


供快速入门，然后学习solr 也简单多了。
  
    
## 加入QQ群[514685454](http://shang.qq.com/wpa/qunwpa?idkey=fd9178627c63f9c0ac0d86a56abdcbdc849043881c30f0cf6002a24ada8f62aa)
交流技术问题，下载项目文档和一键启动依赖服务工具。


